#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * Copyright (c) 2015, Laurent Verdoïa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *     may be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package ${package};

import java.math.BigInteger;

/**
 * Hello world!
 */
public final class App {

    /**
     * Integer max value expressed with an BigInteger.
     * <p/>
     * Used for overflow detection.
     */
    private static final BigInteger INT_MAX_VALUE =
            BigInteger.valueOf(Integer.MAX_VALUE);

    /**
     * Integer min value expressed with an BigInteger.
     * <p/>
     * Used for underflow detection.
     */
    private static final BigInteger INT_MIN_VALUE =
            BigInteger.valueOf(Integer.MIN_VALUE);

    /**
     * Hide constructor of this utility class.
     */
    private App() {
    }

    /**
     * App entry point.
     *
     * @param args Not used
     */
    public static void main(final String[] args) {
        System.out.println("Hello World!");
    }

    /**
     * Add two integer.
     *
     * @param left left operand
     * @param right right operand
     * @return Result of left to right addition
     * @throws ArithmeticException When result cannot be expressed with an
     * integer
     */
    public static int compute(final int left, final int right)
            throws ArithmeticException {
        final BigInteger bigLeft = BigInteger.valueOf(left);
        final BigInteger bigRight = BigInteger.valueOf(right);
        final BigInteger bigResult = bigLeft.add(bigRight);

        if (bigResult.compareTo(INT_MAX_VALUE) > 0) {
            throw new ArithmeticException("Integer overflow: "
                    + bigResult.toString());
        }
        if (bigResult.compareTo(INT_MIN_VALUE) < 0) {
            throw new ArithmeticException("Integer underflow: "
                    + bigResult.toString());
        }

        return bigResult.intValue();
    }
}
